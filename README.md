# Address formatter

## Description

Provides a new field formatter (`address_html`) for the `address` field. 
This module allows you to override address format for specific country or 
for specific country for specific language through admin panel.

## Requirements

* [Address](https://www.drupal.org/project/address)

## Installation

* First you need to install [Address](https://www.drupal.org/project/address) module 
and all it's dependencies.
* Next enable the module as usual.

## Configuration

Module ships with a simple configuration UI which allows you to create, edit
and delete options to the Address field. Enable it and navigate to /admin/config/address-formatter,
after instalation you will see `Default` entity options there. You can use it for your needs.



## Maintainers

* Igor Semenyuk (@semjuel) https://www.drupal.org/user/2426534

## This project has been sponsored by:

* [Rolique](https://www.drupal.org/rolique)
* [Drupal Ukraine Community](https://www.drupal.org/drupal-ukraine-community)
